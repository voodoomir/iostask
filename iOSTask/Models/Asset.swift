//
//  Asset.swift
//  iOSTask
//
//  Created by Vladimir Gradev on 21.03.18.
//  Copyright © 2018 Vladimir Gradev. All rights reserved.
//

import UIKit

enum AssetState {
    case new, done, failed
}

class Asset: Codable {
    let id: String
    let urlString: String?
    var state: AssetState = .new
    var image = UIImage(named: "Placeholder")
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case urlString = "url_q"
    }
}

extension Asset {
    var url: URL? {
        guard let urlStr = urlString else { return nil }
        return URL(string: urlStr)
    }
}
