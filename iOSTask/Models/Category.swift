//
//  Category.swift
//  iOSTask
//
//  Created by Vladimir Gradev on 21.03.18.
//  Copyright © 2018 Vladimir Gradev. All rights reserved.
//

import Foundation


struct Category: Codable {
    let title: String
    let assets: [Asset]
}

extension Category: Equatable{
    static func ==(lhs: Category, rhs: Category) -> Bool {
        return lhs.title == rhs.title
    }
    
    
}
