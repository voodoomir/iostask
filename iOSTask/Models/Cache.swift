//
//  Datasource.swift
//  iOSTask
//
//  Created by Vladimir Gradev on 21.03.18.
//  Copyright © 2018 Vladimir Gradev. All rights reserved.
//

import Foundation

struct Cache {
    
    static func get() -> [Category] {
        let cats: [Category]? = UserDefaults.standard.codableObject(key: "cache")
        return  cats ?? []
    }
    
    static func set( _ cats: [Category]) -> Void {
        UserDefaults.standard.setCodableObject(cats, forKey: "cache")
        UserDefaults.standard.synchronize()
    }
    
    static func writeToFile(data:Data, to name:String) {
        if let url = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first?.appendingPathComponent(name) {
            try? data.write(to: url, options: .atomic)
        }
    }
    
    static func readFromFile(name: String) -> Data? {
        guard let dir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return nil }
        return  try? Data.init(contentsOf: dir.appendingPathComponent(name))
    }
    
}
