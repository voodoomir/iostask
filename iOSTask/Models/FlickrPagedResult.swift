//
//  FlickrPagedResult.swift
//  iOSTask
//
//  Created by Vladimir Gradev on 21.03.18.
//  Copyright © 2018 Vladimir Gradev. All rights reserved.
//

import Foundation

struct FlickrPagedResult: Codable {
    let assets : [Asset]
    enum CodingKeys: String, CodingKey {
        case assets = "photo"
    }
}
