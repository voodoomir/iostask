//
//  Navigator.swift
//  iOSTask
//
//  Created by Vladimir Gradev on 21.03.18.
//  Copyright © 2018 Vladimir Gradev. All rights reserved.
//

import UIKit

class Navigator {
    lazy private var mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
    enum Segue {
        case home
    }
    
    func show(segue: Segue, sender: UIViewController, modal: Bool = false, first: Bool = false, rootVC: Bool = false) {
        switch segue {
        case .home:
            show(target: ImagesVC.createWith(navigator: self, storyboard: mainStoryboard, viewModel: ImagesViewModel()), sender: sender, modal: modal,first: first, rootVC: rootVC)
        }
    }
    
    private func show(target: UIViewController, sender: UIViewController, modal:Bool, first:Bool, rootVC:Bool) {
        
        DispatchQueue.main.async {
            
            guard !rootVC else { UIApplication.shared.delegate?.window??.rootViewController = target; return  }
            
            if let nav = sender as? UINavigationController {
                if !first {
                    nav.pushViewController(target, animated: false)
                } else {
                    nav.viewControllers = [target]
                }
                return
            }
            
            if let nav = sender.navigationController, !modal {
                if !first {
                    nav.pushViewController(target, animated: true)
                } else {
                    nav.viewControllers = [target]
                }
            } else {
                if !first {
                    sender.present(target, animated: false, completion: nil)
                } else {
                    sender.present(UINavigationController(rootViewController: target), animated: false, completion: nil)
                }
            }
        }
    }
}
