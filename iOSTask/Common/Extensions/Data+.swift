//
//  Data+.swift
//  iOSTask
//
//  Created by Vladimir Gradev on 21.03.18.
//  Copyright © 2018 Vladimir Gradev. All rights reserved.
//

import Foundation

extension Data {
    
    var string: String {
        if let s = String(data: self, encoding: String.Encoding.utf8) {
            return s
        }
        return ""
    }
    
    var json: [String : Any]? {
        var ret: [String : Any]?
        do {
            ret = try JSONSerialization.jsonObject(with: self, options: JSONSerialization.ReadingOptions.mutableLeaves)  as? [String : Any]
        }
        catch {
            ret = nil
        }
        return ret
    }
}
