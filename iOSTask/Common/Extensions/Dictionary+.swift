//
//  Dictionary+.swift
//  iOSTask
//
//  Created by Vladimir Gradev on 21.03.18.
//  Copyright © 2018 Vladimir Gradev. All rights reserved.
//

extension Dictionary {
    var queryString: String? {
        return self.reduce("") { "\($0!)\($1.0)=\($1.1)&" }
    }
}
