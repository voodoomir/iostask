//
//  UserDefaults+.swift
//  iOSTask
//
//  Created by Vladimir Gradev on 21.03.18.
//  Copyright © 2018 Vladimir Gradev. All rights reserved.
//

import Foundation

extension UserDefaults {
    func setCodableObject<Value: Encodable>(_ obj:Value, forKey: String) {
        if let data = try? PropertyListEncoder().encode(obj) {
            UserDefaults.standard.set(data, forKey: forKey)
            UserDefaults.standard.synchronize()
        }
    }
    
    func codableObject<Value:Decodable>(key: String )->Value? {
        guard let data = UserDefaults.standard.data(forKey: key) else { return nil }
        return try? PropertyListDecoder().decode(Value.self, from: data)
    }
}
