//
//  UIImageView+.swift
//  iOSTask
//
//  Created by Vladimir Gradev on 21.03.18.
//  Copyright © 2018 Vladimir Gradev. All rights reserved.
//

import UIKit

extension UIImageView {
    static let activityTag: Int = 454
    fileprivate var activityIndicator: UIActivityIndicatorView {
        get {
            if let existingIndicator = self.viewWithTag(UIImageView.activityTag)  as? UIActivityIndicatorView {
                return existingIndicator
            } else {
                let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
                activityIndicator.hidesWhenStopped = true
                activityIndicator.center = CGPoint(x:self.frame.width/2,y: self.frame.height/2)
                activityIndicator.stopAnimating()
                activityIndicator.tag = UIImageView.activityTag
                self.addSubview(activityIndicator)
                return activityIndicator
            }
        }
    }
    
    func showActivityIndicator() {
        self.activityIndicator.startAnimating()
    }
    
    func stopActivityIndicator() {
        self.activityIndicator.stopAnimating()
    }
}
