//
//  UIViewController+.swift
//  iOSTask
//
//  Created by Vladimir Gradev on 21.03.18.
//  Copyright © 2018 Vladimir Gradev. All rights reserved.
//

import UIKit

extension UIViewController {
    var activityIndicatorTag: Int { return 77777 }

    func startActivityIndicator( style: UIActivityIndicatorViewStyle = .gray, location: CGPoint? = nil) {
        
        let loc = location ?? self.view.center
        
        DispatchQueue.main.async(){
            
            let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: style)
            activityIndicator.transform = CGAffineTransform(scaleX: 2,y: 2)
            activityIndicator.tag = self.activityIndicatorTag
            
            activityIndicator.center = loc
            activityIndicator.hidesWhenStopped = true
            
            activityIndicator.startAnimating()
            self.view.addSubview(activityIndicator)
        }
    }

    func stopActivityIndicator() {
        
        DispatchQueue.main.async() {
            if let activityIndicator = self.view.subviews.filter(
                { $0.tag == self.activityIndicatorTag}).first as? UIActivityIndicatorView {
                activityIndicator.stopAnimating()
                activityIndicator.removeFromSuperview()
            }
        }
    }

}
