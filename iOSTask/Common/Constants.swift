//
//  Constants.swift
//  iOSTask
//
//  Created by Vladimir Gradev on 21.03.18.
//  Copyright © 2018 Vladimir Gradev. All rights reserved.
//

import UIKit

let APP_CONF = ServiceConfiguration(baseURL:Environments.prod.rawValue)

enum Environments: String {
    case prod =     "https://api.flickr.com/"
}

let DEFAULT_SEARCH_CATEGORY = "cats"
let FLICKR_KEY = "bdb93d0588c40454b6164f86d26dd055"
let FLICKR_SECRET = "e776284a13d482af"
let IMAGES_CONTAINER_HEIGHT:CGFloat = 250.0
var THUMB_LAYOUT:UICollectionViewLayout {
    let layout = UICollectionViewFlowLayout()
    layout.scrollDirection = .horizontal
    layout.itemSize = CGSize(width: 100.0 , height:150.0)
    return layout
}
