//
//  ThumbnailsViewController.swift
//  iOSTask
//
//  Created by Vladimir Gradev on 21.03.18.
//  Copyright © 2018 Vladimir Gradev. All rights reserved.
//

import UIKit
import RxSwift
class ThumbnailsViewController: UIViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    private let disposeBag = DisposeBag()
    var viewModel: ThumbnailsViewModelable!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var removeButton: UIButton!
    static func createWith(storyboard: UIStoryboard?, viewModel: ThumbnailsViewModelable) -> ThumbnailsViewController? {
        guard let storyboard = storyboard else { return nil }
        return storyboard.instantiateViewController(ofType: ThumbnailsViewController.self).then { vc in
            vc.viewModel = viewModel
        }
    }
    
    lazy var downloadsInProgress = [IndexPath:Operation]()
    lazy var downloadQueue: OperationQueue = {
        var queue = OperationQueue()
        queue.name = "img_get_queue"
        queue.maxConcurrentOperationCount = 15
        return queue
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.setCollectionViewLayout(THUMB_LAYOUT, animated: false)
        
        viewModel.datasource.asObservable().bind(to: collectionView.rx.items){ (collectionView, row, asset) in
            let indexPath = IndexPath(row: row, section: 0)
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ThumbnailCell.id, for: indexPath) as! ThumbnailCell
            cell.imgView.stopAnimating()
            
            if !self.checkCacheFor(asset: asset, at: indexPath) {
                switch asset.state {
                    case .failed:
                        print("getting image failed")
                    case .new, .done:
                        cell.imgView.startAnimating()
                        self.prepareOperationsFor(asset: asset, indexPath: indexPath)
                }
            }
            
            cell.imgView.image = asset.image
            
            return cell
        }.disposed(by: disposeBag)
        
        viewModel.title.asObservable().subscribe(onNext:{ [weak self] title in
            self?.titleLabel.text = title
        }).disposed(by: self.disposeBag)
    }
    
    private func checkCacheFor(asset: Asset, at indexPath: IndexPath) -> Bool {
        if let existingFile = Cache.readFromFile(name: asset.id) {
            asset.state = .done
            asset.image = UIImage(data: existingFile)
            return true
        }
        
        return false
    }

    private func prepareOperationsFor(asset: Asset, indexPath: IndexPath){
        switch asset.state {
            case .new:
                startDownloadForAsset(asset, indexPath: indexPath)
            default:
                break;
        }
    }
    
    private func startDownloadForAsset( _ asset: Asset, indexPath: IndexPath){

        if let _ = downloadsInProgress[indexPath] {  return  }
        
        let getter = ImageGetter(asset: asset)
 
        getter.completionBlock = { [weak self] in
            guard !getter.isCancelled, let sSelf = self else { return }
            
            if let image = asset.image, let imgData = UIImagePNGRepresentation(image) {
                Cache.writeToFile(data: imgData, to: asset.id)
            }
            
            DispatchQueue.main.async {
                sSelf.downloadsInProgress.removeValue(forKey: indexPath)
                sSelf.collectionView?.reloadItems(at: [indexPath])
            }
        }
      
        downloadsInProgress[indexPath] = getter
        downloadQueue.addOperation(getter)
    }
}
