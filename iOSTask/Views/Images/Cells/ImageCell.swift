//
//  ImageCell.swift
//  iOSTask
//
//  Created by Vladimir Gradev on 21.03.18.
//  Copyright © 2018 Vladimir Gradev. All rights reserved.
//

import UIKit
import RxSwift
class ImageCell: UICollectionViewCell {
    static let id: String = "ImageCell"
    var disposeBag = DisposeBag()
    var thumbnailVC: ThumbnailsViewController? = nil
    
    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag()
    }
}
