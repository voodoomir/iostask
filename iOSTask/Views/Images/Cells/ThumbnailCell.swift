//
//  ThumbnailCell.swift
//  iOSTask
//
//  Created by Vladimir Gradev on 21.03.18.
//  Copyright © 2018 Vladimir Gradev. All rights reserved.
//

import UIKit

class ThumbnailCell: UICollectionViewCell {
    static let id = "ThumbnailCell"
    
    @IBOutlet weak var imgView: UIImageView!
}
