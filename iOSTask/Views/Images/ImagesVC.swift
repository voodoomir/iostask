//
//  ImagesVC.swift
//  iOSTask
//
//  Created by Vladimir Gradev on 21.03.18.
//  Copyright © 2018 Vladimir Gradev. All rights reserved.
//

import RxSwift
import Then

class ImagesVC: UIViewController {
    
    var navigator: Navigator!
    var viewModel: ImagesViewModelable!
    
    let disposeBag = DisposeBag()
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var searchTF: UITextField!
    
    static func createWith(navigator: Navigator = Navigator(), storyboard: UIStoryboard, viewModel: ImagesViewModelable) -> ImagesVC {
        return storyboard.instantiateViewController(ofType: ImagesVC.self).then { vc in
            vc.navigator = navigator
            vc.viewModel = viewModel
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Flickr iOS Task"
        let cellSize = CGSize(width: collectionView.frame.width , height:IMAGES_CONTAINER_HEIGHT)
        
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.itemSize = cellSize
        layout.sectionInset = UIEdgeInsets(top: 1, left: 1, bottom: 1, right: 1)
        layout.minimumLineSpacing = 50.0
        layout.minimumInteritemSpacing = 1.0
        self.collectionView.setCollectionViewLayout(layout, animated: false)
        
        viewModel.datasource.asObservable().bind(to: collectionView.rx.items){ (collectionView, row, category) in
            let indexPath = IndexPath(row: row, section: 0)
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ImageCell.id, for: indexPath) as! ImageCell
            
            if cell.thumbnailVC == nil,  let thumbsViewController = ThumbnailsViewController.createWith(storyboard: self.storyboard, viewModel: ThumbnailsViewModel(category)) {
                self.addChildViewController(thumbsViewController)
                thumbsViewController.view.frame = cell.bounds
                cell.addSubview(thumbsViewController.view)
                thumbsViewController.didMove(toParentViewController: self)
                cell.thumbnailVC = thumbsViewController
                cell.isUserInteractionEnabled  = true
                cell.thumbnailVC?.view.isUserInteractionEnabled  = true
            } else {
                cell.thumbnailVC?.viewModel.newCategory(category)
            }
            
            if let thumbVC = cell.thumbnailVC {
                thumbVC.removeButton.rx.tap.subscribe(onNext:{ [weak self] in
                    self?.viewModel.removeCategory(category)
                }).disposed(by: cell.disposeBag)
            }
            
            
            return cell
        }.disposed(by: disposeBag)
        
        if !UserDefaults.standard.bool(forKey: "notFirstRun") {
            UserDefaults.standard.setValue(true, forKey: "notFirstRun")
            search(txt: DEFAULT_SEARCH_CATEGORY)
        }
    }
    
    private func search(txt: String) {
        self.startActivityIndicator()
        viewModel.getImages(search: txt).subscribe(onNext: {[weak self] (s) in
            self?.stopActivityIndicator()
        }).disposed(by: self.disposeBag)
    }
    
    @IBAction func searchTapped(_ sender: UIButton) {
        guard let searchText = searchTF.text, !searchText.isEmpty else { print("Please enter some text"); return }
        searchTF.text = ""
        searchTF.resignFirstResponder()
        search(txt: searchText)
    }
}
