//
//  NetworkTask.swift
//  iOSTask
//
//  Created by Vladimir Gradev on 21.03.18.
//  Copyright © 2018 Vladimir Gradev. All rights reserved.
//

import RxSwift

class NetworkTask<Input: Request, Output>: Task<Input, Output> {
    let dispatcher: NetworkDispatcherable
    
    init(dispatcher: NetworkDispatcherable = Service()) {
        self.dispatcher = dispatcher
    }
    
    override func perform(_ element: Input) -> Observable<Output> {
        fatalError("not implemented")
    }
}
