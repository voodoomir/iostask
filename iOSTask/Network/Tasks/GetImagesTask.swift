//
//  API+Images.swift
//  iOSTask
//
//  Created by Vladimir Gradev on 21.03.18.
//  Copyright © 2018 Vladimir Gradev. All rights reserved.
//

import Foundation
import RxSwift

//MARK: -
//MARK: Get Images
//MARK: -
struct GetImagesRequest: Request {
    var params: [String : Any]?
    let path: String = "services/rest"
    var method = HTTPMethod.POST
    
    init(search: String = DEFAULT_SEARCH_CATEGORY, key:String = FLICKR_KEY, secret:String = FLICKR_SECRET, extras:String = "url_q,owner_name", format:String = "json", method:String = "flickr.photos.search", noJsonCallback:Bool = true ) {
        var newParams = [String:Any]()
        newParams["api_key"] = key
        newParams["extras"] = extras
        newParams["format"] = format
        newParams["text"] = search
        newParams["method"] = method
        newParams["nojsoncallback"] = noJsonCallback ? "1" : "0"
        params = newParams
    }
}

class GetImagesTask: NetworkTask<GetImagesRequest,FlickrResult?> {
    override func perform(_ req: GetImagesRequest = GetImagesRequest()) -> Observable<FlickrResult?>{
        
        return self.dispatcher.execute(request: req).flatMap({ (result) -> Observable<FlickrResult?> in
            return Observable.just(  try? JSONDecoder().decode(FlickrResult.self, from: result.data) )
        })
    }
}
