//
//  Request.swift
//  iOSTask
//
//  Created by Vladimir Gradev on 21.03.18.
//  Copyright © 2018 Vladimir Gradev. All rights reserved.
//

enum HTTPMethod: String {  case GET, POST, DELETE, PUT, PATCH }


protocol Request {
    var path: String { get }
    var method: HTTPMethod { get }
    var params: [String: Any]? { get }
    var headers: [String: String]? { get }
    
}

extension Request {
    var method: HTTPMethod { return .GET }
    var params: [String: Any]? { return nil }
    var headers: [String: String]? { return nil }
}
