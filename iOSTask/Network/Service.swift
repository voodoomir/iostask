//
//  Service.swift
//  iOSTask
//
//  Created by Vladimir Gradev on 21.03.18.
//  Copyright © 2018 Vladimir Gradev. All rights reserved.
//

import RxSwift
import RxCocoa

protocol NetworkDispatcherable {
    func execute(request: Request) -> Observable<(response: HTTPURLResponse, data: Data)>
}

class Service:NSObject,NetworkDispatcherable, URLSessionDelegate {
    
    let conf:ServiceConfiguration
    
    init(conf:ServiceConfiguration = APP_CONF){
        self.conf = conf
    }
    
    func execute(request: Request) -> Observable<(response: HTTPURLResponse, data: Data)>  {
        
        return Observable.of(request)
            .map{ req -> URL in return URL(string: "\(self.conf.baseURL)\(req.path)")!  }
            .map{ url in
                
                var urlRequest:URLRequest
                if let paramsString = request.params?.queryString, let newUrl = URL(string: "\(url.absoluteString)?\(paramsString)") {
                    urlRequest =  URLRequest(url: newUrl)
                } else {
                    urlRequest =  URLRequest(url: url)
                }
                urlRequest.httpMethod = request.method.rawValue

                request.headers?.forEach{ (key,value) in
                    urlRequest.addValue(value, forHTTPHeaderField: key)
                }
                
                return urlRequest
            }.flatMap{ request -> Observable<(response: HTTPURLResponse, data: Data)> in
                return  URLSession.shared.rx.response(request: request)
            }
    }
}
