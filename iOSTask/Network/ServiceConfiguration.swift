//
//  ServiceConfiguration.swift
//  iOSTask
//
//  Created by Vladimir Gradev on 21.03.18.
//  Copyright © 2018 Vladimir Gradev. All rights reserved.
//

import Foundation

struct ServiceConfiguration {
    let baseURL: String
}
