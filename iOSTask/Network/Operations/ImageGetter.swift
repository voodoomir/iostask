//
//  ImageGetter.swift
//  iOSTask
//
//  Created by Vladimir Gradev on 21.03.18.
//  Copyright © 2018 Vladimir Gradev. All rights reserved.
//

import UIKit

class ImageGetter: Operation {
    
    let asset: Asset
    init(asset: Asset) {
        self.asset = asset
    }
    
    override func main() {
        guard !self.isCancelled else { return }
        guard let assetUrl = asset.url else {  failAsset(); return  }
        
        let imageData = try? Data.init(contentsOf: assetUrl)
        
        guard !self.isCancelled else { return }
        
        if let imgData = imageData {
            self.asset.image = UIImage(data: imgData)
            self.asset.state = .done
        }  else  {
            failAsset()
        }
    }
    
    private func failAsset() {
        self.asset.state = .failed
        self.asset.image = UIImage(named: "Error")
    }
}
