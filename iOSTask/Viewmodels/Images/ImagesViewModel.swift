//
//  ImagesViewModel.swift
//  iOSTask
//
//  Created by Vladimir Gradev on 21.03.18.
//  Copyright © 2018 Vladimir Gradev. All rights reserved.
//

import Foundation
import RxSwift

struct ImagesViewModel: ImagesViewModelable {
    
    let y = Cache.get()
    
    var datasource = Variable<[Category]>(Cache.get())
    

    
    func getImages(search: String) -> Observable<Bool> {
        return GetImagesTask().perform(GetImagesRequest(search: search)).flatMap({ (fRes) -> Observable<Bool> in
            guard let flickResult = fRes, let page = flickResult.page  else { return Observable.just(false) }
            
            if page.assets.count > 0 {
                self.datasource.value.insert(Category(title: search, assets: page.assets), at: 0)
                Cache.set(self.datasource.value)
            }
            
            return Observable.just(true)
        })
    }
    
    func removeCategory(_ cat: Category) {
        datasource.value.removeObject(cat)
        Cache.set(self.datasource.value)
    }
}
