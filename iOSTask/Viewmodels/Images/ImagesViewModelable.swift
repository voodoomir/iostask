//
//  ImagesViewModelable.swift
//  iOSTask
//
//  Created by Vladimir Gradev on 21.03.18.
//  Copyright © 2018 Vladimir Gradev. All rights reserved.
//

import Foundation
import RxSwift

protocol ImagesViewModelable {
    var datasource: Variable<[Category]> { get }
    func getImages(search: String) -> Observable<Bool>
    func removeCategory(_ cat: Category)
}
