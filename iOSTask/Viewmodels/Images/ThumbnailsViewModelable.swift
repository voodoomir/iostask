//
//  ThumbnailsViewModelable.swift
//  iOSTask
//
//  Created by Vladimir Gradev on 21.03.18.
//  Copyright © 2018 Vladimir Gradev. All rights reserved.
//

import RxSwift

protocol ThumbnailsViewModelable {
    var datasource: Variable<[Asset]> { get }
    var title: Variable<String> { get }
    func newCategory(_ cat: Category)
}
