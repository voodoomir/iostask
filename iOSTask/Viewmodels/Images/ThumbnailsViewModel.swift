//
//  ThumbnailsViewModel.swift
//  iOSTask
//
//  Created by Vladimir Gradev on 21.03.18.
//  Copyright © 2018 Vladimir Gradev. All rights reserved.
//

import Foundation
import RxSwift

struct ThumbnailsViewModel: ThumbnailsViewModelable {
    var datasource = Variable<[Asset]>([])
    var title = Variable<String>("")
    
    init(_ cat: Category) {
        newCategory(cat)
    }
    
    func newCategory(_ cat: Category) {
        datasource.value = cat.assets
        title.value = cat.title
    }
}
